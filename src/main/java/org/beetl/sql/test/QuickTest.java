package org.beetl.sql.test;

import java.util.HashMap;
import java.util.List;

import org.beetl.sql.core.ClasspathLoader;
import org.beetl.sql.core.DefaultNameConversion;
import org.beetl.sql.core.Interceptor;
import org.beetl.sql.core.SQLLoader;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.ext.gen.GenConfig;

public class QuickTest {

	public static void main(String[] args) throws Exception{
		MySqlStyle style = new MySqlStyle();
		MySqlConnectoinSource cs = new MySqlConnectoinSource();
		SQLLoader loader = new ClasspathLoader("/sql");
		SQLManager sql = new SQLManager(style,loader,cs,new UnderlinedNameConversion(), new Interceptor[]{new DebugInterceptor()});
		
//		sql.select("appliction.findAllByPage", SysRole.class, new HashMap());
		
		//		sql.unique(SysRole.class, 1);
//		sql.all(SysRole.class);
//		sql.allCount(SysRole.class);
//		sql.deleteById(SysRole.class, 10000);
		{
//			SysRole role = new SysRole();
//			role.setName("aaabb");
//			role.setCompanyId(1);
////			sql.insert(SysRole.class, role);
//			KeyHolder kh = new KeyHolder();
//			sql.insert(SysRole.class, role, kh);
//			System.out.println(kh.getKey());
			
		}
		{
//			SysRole role = new SysRole();
//			role.setName("aaabb");
//			role.setCompanyId(1);
//			sql.template(role);
			
		}
		{
//			SysRole role = new SysRole();
//			role.setId(1);
//			role.setName("admin");
//			sql.updateById(role);
		
		}
		
		{
//			List<SysRole> list = sql.all(SysRole.class, 1, 9);
//			System.out.println(list.size());
//			
		}
		
		
		sql = new SQLManager(style,loader,cs,new DefaultNameConversion(), new Interceptor[]{new DebugInterceptor()});
//		sql.unique(UserInfo.class, 1);
//		UserInfo info = new UserInfo();
//		info.setUserName("name");
//		sql.template(info);
		
		sql.genPojoCodeToConsole("sys_parametr_info");
//		GenConfig config = new GenConfig();
//		config.preferBigDecimal(true);
////		config.setBaseClass("com.test.User");
//		sql.genPojoCode("sys_role","com.test",config);

	}

}
